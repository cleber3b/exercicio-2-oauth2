package br.com.oauth2.oauth2Exercicio2.security;

import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;

import java.util.Map;

public class UsuarioPrincipalExtractor implements PrincipalExtractor {

    @Override
    public Object extractPrincipal(Map<String, Object> map) {
        UsuarioLogado usuario = new UsuarioLogado();

        usuario.setId((Integer) map.get("id"));
        usuario.setNome((String) map.get("name"));

        return usuario;
    }
}
