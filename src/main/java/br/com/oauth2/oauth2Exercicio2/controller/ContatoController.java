package br.com.oauth2.oauth2Exercicio2.controller;

import br.com.oauth2.oauth2Exercicio2.model.Contato;
import br.com.oauth2.oauth2Exercicio2.model.dto.ContatoMapper;
import br.com.oauth2.oauth2Exercicio2.model.dto.CriarContatoRequest;
import br.com.oauth2.oauth2Exercicio2.model.dto.CriarContatoResponse;
import br.com.oauth2.oauth2Exercicio2.model.dto.GetContatosResponse;
import br.com.oauth2.oauth2Exercicio2.security.UsuarioLogado;
import br.com.oauth2.oauth2Exercicio2.service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class ContatoController {

    @Autowired
    ContatoService contatoService;

    @Autowired
    ContatoMapper contatoMapper;

    @PostMapping("/contato")
    public CriarContatoResponse crairUsuario(@RequestBody @Valid CriarContatoRequest criarContatoRequest, @AuthenticationPrincipal UsuarioLogado usuarioLogado) {
        return contatoMapper.paraCriarContatoResponse(contatoService.criarContato(contatoMapper.paraContato(criarContatoRequest), usuarioLogado));
    }

    @GetMapping("/contatos")
    public GetContatosResponse listarContatos(@AuthenticationPrincipal UsuarioLogado usuarioLogado){
        return contatoMapper.paraContatosUsuarioResponse(contatoService.listarContatos(usuarioLogado), usuarioLogado);
    }
}
