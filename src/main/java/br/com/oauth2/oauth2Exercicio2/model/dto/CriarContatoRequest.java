package br.com.oauth2.oauth2Exercicio2.model.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CriarContatoRequest {

    @NotNull(message ="Nome precisa ser preenchido.")
    @Size(min = 3, message = "Nome precisa ter no mínimo 3 letras.")
    @NotBlank(message = "Nome precisa ser preenchido.")
    private String nome;

    @Email(message = "Email inválido.")
    private String email;

    private String telefone;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
}
