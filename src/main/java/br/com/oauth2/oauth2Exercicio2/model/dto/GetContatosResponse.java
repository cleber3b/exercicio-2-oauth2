package br.com.oauth2.oauth2Exercicio2.model.dto;

import br.com.oauth2.oauth2Exercicio2.model.Contato;

import java.util.List;

public class GetContatosResponse {

    private Integer idUsuario;

    private String nomeUsuario;

    private List<Contato> contatos;

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public List<Contato> getContatos() {
        return contatos;
    }

    public void setContatos(List<Contato> contatos) {
        this.contatos = contatos;
    }
}
