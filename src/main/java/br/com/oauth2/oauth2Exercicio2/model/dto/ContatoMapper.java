package br.com.oauth2.oauth2Exercicio2.model.dto;

import br.com.oauth2.oauth2Exercicio2.model.Contato;
import br.com.oauth2.oauth2Exercicio2.security.UsuarioLogado;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ContatoMapper {

    public Contato paraContato(CriarContatoRequest criarContatoRequest){
        Contato contato = new Contato();
        contato.setNome(criarContatoRequest.getNome());
        contato.setEmail(criarContatoRequest.getEmail());
        contato.setTelefone(criarContatoRequest.getTelefone());

        return contato;
    }

    public GetContatosResponse paraContatosUsuarioResponse(List<Contato> listaContatosDoUsuario, UsuarioLogado usuarioLogado) {

        GetContatosResponse contatosUsuarioResponse = new GetContatosResponse();
        contatosUsuarioResponse.setIdUsuario(usuarioLogado.getId());
        contatosUsuarioResponse.setNomeUsuario(usuarioLogado.getNome());
        contatosUsuarioResponse.setContatos(listaContatosDoUsuario);

        return contatosUsuarioResponse;
    }

    public CriarContatoResponse paraCriarContatoResponse(Contato contato){
        CriarContatoResponse criarContatoResponse = new CriarContatoResponse();
        criarContatoResponse.setId(contato.getId());
        criarContatoResponse.setNome(contato.getNome());
        criarContatoResponse.setEmail(contato.getEmail());
        criarContatoResponse.setTelefone(contato.getTelefone());

        return criarContatoResponse;
    }



}
