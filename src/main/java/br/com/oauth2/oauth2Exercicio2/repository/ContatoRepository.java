package br.com.oauth2.oauth2Exercicio2.repository;

import br.com.oauth2.oauth2Exercicio2.model.Contato;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContatoRepository extends CrudRepository<Contato, Integer> {

    List<Contato> findByIdUsuario(Integer idUsuario);

}
