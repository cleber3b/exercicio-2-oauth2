package br.com.oauth2.oauth2Exercicio2.service;

import br.com.oauth2.oauth2Exercicio2.model.Contato;
import br.com.oauth2.oauth2Exercicio2.repository.ContatoRepository;
import br.com.oauth2.oauth2Exercicio2.security.UsuarioLogado;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContatoService {

    @Autowired
    ContatoRepository contatoRepository;

    public Contato criarContato(Contato contato, UsuarioLogado usuarioLogado) {

        contato.setIdUsuario(usuarioLogado.getId());
        return contatoRepository.save(contato);
    }

    public List<Contato> listarContatos(UsuarioLogado usuarioLogado) {
        return contatoRepository.findByIdUsuario(usuarioLogado.getId());
    }
}
