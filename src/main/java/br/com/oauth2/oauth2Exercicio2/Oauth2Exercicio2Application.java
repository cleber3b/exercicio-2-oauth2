package br.com.oauth2.oauth2Exercicio2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Oauth2Exercicio2Application {

	public static void main(String[] args) {
		SpringApplication.run(Oauth2Exercicio2Application.class, args);
	}

}
